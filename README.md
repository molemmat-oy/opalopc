# OpalOPC

This CI/CD job definition runs a OpalOPC vulnerability scan against a target server. On completion, an HTML and a SARIF report is generated containing information about the vulnerabilities found, where they were located, additional information about the vulnerability and links to our learning resources with suggestions on how to fix them.

## About OpalOPC

- [OpalOPC](https://opalopc.com/) is a OPC UA security scanner.
- It is designed to be run manually by security testers, or via a CI/CD pipeline by developers
- It checks your OPC UA server for [various security issues](https://opalopc.com/docs/category/plugins) that are likely to interest you during software development, as well as attackers during a cyberattack.

## Inputs

## `target-url`

**Required** The full URL (including scheme) of the server to scan.

## `license-key`

**Required** OpalOPC license key.

## `output-base-filename`

**Optional** The base filename used for the scan report. This will be stored in the GITHUB_WORKSPACE (/github/workspace) directory. The resulting reports have suffixes `.html` and `.sarif`.

**Default** `opalopc-report`

## Adding the scanner to your GitLab CI/CD pipeline

Activate this security scanner by adding the following to your [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/):

```yaml
include:
  - component: gitlab.com/molemmat-oy/opalopc/opalopc@main
    inputs:
      target-url: <set-your-target-opc-uri-here>
      license-key: $OPALOPC_LICENSE_KEY
```

You must set the variable `OPALOPC_LICENSE_KEY` with your license key. This is a secret, [set it in the UI](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui).

### Example

```yaml
include:
  - component: gitlab.com/molemmat-oy/opalopc/opalopc@main
    inputs:
      target-url: opc.tcp://scanme.opalopc.com:53530
      license-key: $OPALOPC_LICENSE_KEY
```

---

Created and maintained by [Valtteri Lehtinen](https://github.com/ValtteriL)🌿

- [Blog](https://shufflingbytes.com)
- [LinkedIn](https://www.linkedin.com/in/valtterilehtinen/)
